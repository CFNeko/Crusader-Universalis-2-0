﻿name_list_ainu = {
	cadet_dynasty_names = {
		"dynn_anchikar"
		"dynn_kamuy"
		"dynn_morueran"
		"dynn_naiporo"
		"dynn_nitai"
		"dynn_nupurpet"
		"dynn_nupuri"
		"dynn_nupitai"
		"dynn_nupurikotan"
		"dynn_petkotan"
		"dynn_ponkotan"
		"dynn_rusut"
		"dynn_satporopet"
		"dynn_shikot"
		"dynn_toya"
		"dynn_upash"
		"dynn_upashkot"
	}

	dynasty_names = {
		"dynn_anchikar"
		"dynn_kamuy"
		"dynn_morueran"
		"dynn_naiporo"
		"dynn_nitai"
		"dynn_nupurpet"
		"dynn_nupuri"
		"dynn_nupitai"
		"dynn_nupurikotan"
		"dynn_petkotan"
		"dynn_ponkotan"
		"dynn_rusut"
		"dynn_satporopet"
		"dynn_shikot"
		"dynn_toya"
		"dynn_upash"
		"dynn_upashkot"
	}

	male_names = {
	Apasam_Kamuy Chikap Hakumakle Harukor Isonash Kamuy Kamuima Kandakoro_Kamuy Kim_un_Kamuy Kinalabukk Kina_Sut_Kamuy Kotan_Kar_Kamuy Kuuchinklo Kuuklekle Mosirkara_Kamuy Nusa_Kor_Kamuy Pauchi_Kamuy Shinda Shiramba_Kamuy Turushino Turushno 
	}
	female_names = {
	Abenanka Hakumakle Hasinaw__Uk_Kamuy Imekanu Kakula Kamuy Kenas_Unarpe Kinalabukk Muisashimats Niutanimats Resunotek Tanelankemats Tokapcup_Kamuy Waka_Ush_Kamuy Yushkep_Kamuy
	}
	
	founder_named_dynasties = no
}