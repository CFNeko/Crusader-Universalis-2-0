﻿swedish = {
	color = { 0.25 0.5 0.75 }

	ethos = ethos_communal
	heritage = heritage_north_germanic
	language = language_swedish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_metal_craftsmanship
		tradition_collective_lands
	}
	
	name_list = name_list_swabian
	
	coa_gfx = { swedish_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}
norwegian = {
	color = { 210 150 255 }

	ethos = ethos_communal
	heritage = heritage_north_germanic
	language = language_norwegian
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_mountain_homes
		flag:tradition_seafaring
		tradition_stand_and_fight
	}
	
	name_list = name_list_swabian
	
	coa_gfx = { norwegian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}
danish = {
	color = { 0.15 0.4 0.55 }

	ethos = ethos_communal
	heritage = heritage_north_germanic
	language = language_danish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_collective_lands
		tradition_chivalry
	}
	
	name_list = name_list_swabian
	
	coa_gfx = { danish_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}