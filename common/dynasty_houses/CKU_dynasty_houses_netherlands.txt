﻿###############
# Netherlands #
###############
# Holland (Netherlands)
house_brederode = {
	prefix = "dynnp_van"
	name = "dynn_brederode"
	dynasty = CKU_holland
}
house_vander_duyn = {
	prefix = "dynnp_van_der"
	name = "dynn_vander_duyn"
	dynasty = CKU_holland
}
house_adrichem = {
	prefix = "dynnp_van"
	name = "dynn_adrichem"
	dynasty = CKU_holland
}
house_bentheim = {
	prefix = "dynnp_van"
	name = "dynn_bentheim"
	dynasty = CKU_holland
}
house_bentheim_gotterswick = {
	prefix = "dynnp_von"
	name = "dynn_bentheim_gotterswick"
	dynasty = CKU_holland
}

# Wassenaer
house_duivenvoorde = {
	prefix = "dynnp_van"
	name = "dynn_duivenvoorde"
	dynasty = CKU_wassenaer
}
house_santhorst = {
	prefix = "dynnp_van"
	name = "dynn_santhorst"
	dynasty = CKU_wassenaer
}
house_cranenburch = {
	prefix = "dynnp_van"
	name = "dynn_cranenburch"
	dynasty = CKU_wassenaer
}
house_polanen = {
	prefix = "dynnp_van"
	name = "dynn_polanen"
	dynasty = CKU_wassenaer
}
house_vander_leck = {
	prefix = "dynnp_van_der"
	name = "dynn_leck"
	dynasty = CKU_wassenaer
}
house_vanden_bergh = {
	prefix = "dynnp_van_den"
	name = "dynn_vanden_bergh"
	dynasty = CKU_wassenaer
}

# Horne
house_horne_houtekerke = {
	prefix = "dynnp_van"
	name = "dynn_horne_houtekerke"
	dynasty = CKU_horne
}
house_cranendonck = {
	name = "dynn_horne_cranendonck"
	dynasty = CKU_horne
}

# Brabant
house_withem = {
	prefix = "dynnp_van"
	name = "dynn_withem"
	dynasty = CKU_brabant
}
house_berghes = {
	prefix = "dynnp_van"
	name = "dynn_berghes"
	dynasty = CKU_brabant
}
house_hesse = {
	prefix = "dynnp_van"
	name = "dynn_hesse"
	dynasty = CKU_brabant
}

# Buren
house_buren_caets = {
	prefix = "dynnp_van"
	name = "dynn_buren_caets"
	dynasty = CKU_buren
}

# Zuylen
house_zuylen_abcoude = {
	prefix = "dynnp_van"
	name = "dynn_zuylen_abcoude"
	dynasty = CKU_zuylen
}

# Loon
house_loon_reineck = {
	prefix = "dynnp_von"
	name = "dynn_loon_reineck"
	dynasty = CKU_loon
}