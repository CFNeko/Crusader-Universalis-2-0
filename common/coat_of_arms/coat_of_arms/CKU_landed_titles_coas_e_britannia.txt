@smCastleX = 0.27
@smCastleY = 0.23
@smLysX = 0.23
@smLysY = 0.26
@smCross = 0.22


e_britannia = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "red"
	colored_emblem = {
		texture = "ce_ordinary_cross_thinnest.dds"
		color1 = "white"
		instance = { scale = { 1.35 1.35 } rotation = 45 }
	}
	colored_emblem = {
		texture = "ce_ordinary_cross_thinnest.dds"
		color1 = "red"
		instance = { scale = { 1.175 1.175 } rotation = 45 }	
	}
	colored_emblem = {
		texture = "ce_ordinary_cross_thinnest.dds"
		color1 = "white"
		instance = { scale = { 1.3 1.3 } }	
	}
	colored_emblem = {
		texture = "ce_ordinary_cross_thinnest.dds"
		color1 = "red"
		instance = { scale = { 1.0 1.0 } }	
	}
}

k_scotland = {
	pattern = "pattern_solid.dds"
	color1 = "yellow"
	color2 = "red"
	colored_emblem = {
		texture = "ce_crancelin.dds"
		color1 = "red"
		color2 = "red"
		color3 = "red"
		instance = { position = { 0.5 0.1 } scale = { 1.0 0.5 } rotation = 135 }
		instance = { position = { 0.5 0.9 } scale = { 1.0 0.5 } rotation = -45 }
	}
	colored_emblem = {
		texture = "ce_lion_rampant.dds"
		color1 = "red"
		color2 = "blue"
		color3 = "black"
		instance = { position = { 0.5 0.5 } scale = { 0.8 0.8 } }
	}
}

k_england={
	custom=yes
	pattern="pattern_checkers_02.dds"
	color1=red
	color2=blue
	color3=red
	colored_emblem={
		color1=yellow
		color3=yellow
		texture="ce_fleur.dds"
		instance={
			position={ 0.860000 0.620000 }
			scale={ 0.300000 0.290000 }
			depth=6.010000
		}
	}
	colored_emblem={
		color1=yellow
		color2=blue
		color3=blue
		texture="ce_leopard_passant_guardant.dds"
		instance={
			position={ 0.290000 0.890000 }
			scale={ 0.490000 0.350000 }
		}
		instance={
			position={ 0.270000 0.740000 }
			scale={ 0.550000 0.480000 }
			depth=1.010000
		}
		instance={
			position={ 0.260000 0.580000 }
			scale={ 0.560000 0.450000 }
			depth=2.010000
		}
		instance={
			position={ 0.720000 0.400000 }
			scale={ 0.530000 0.450000 }
			depth=3.010000
		}
		instance={
			position={ 0.730000 0.240000 }
			scale={ 0.530000 0.460000 }
			depth=4.010000
		}
		instance={
			position={ 0.720000 0.090000 }
			scale={ 0.500000 0.420000 }
			depth=5.010000
		}
	}
	colored_emblem={
		color1=yellow
		texture="ce_fleur.dds"
		instance={
			position={ 0.600000 0.620000 }
			scale={ 0.260000 0.270000 }
			depth=7.010000
		}
		instance={
			position={ 0.720000 0.790000 }
			scale={ 0.270000 0.310000 }
			depth=8.010000
		}
		instance={
			position={ 0.160000 0.140000 }
			scale={ 0.270000 0.280000 }
			depth=9.010000
		}
		instance={
			position={ 0.380000 0.150000 }
			scale={ 0.260000 0.280000 }
			depth=10.010000
		}
		instance={
			position={ 0.280000 0.350000 }
			scale={ 0.280000 0.250000 }
			depth=11.010000
		}
	}
}

k_wales = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "green"
	colored_emblem = {
		texture = "ce_lotus_flower.dds"
		color1 = "green"
		instance = { position = { 0.5 0.85 } scale = { 1.25 0.5 } }
	}
	colored_emblem = {
		texture = "ce_dragon.dds"
		color1 = "red"
		color2 = "blue"
		color3 = "white"
		instance = { position = { 0.5 0.5 } scale = { 1.25 1.0 } }
	}
}

k_ireland = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_harp.dds"
		color1 = "yellow"
	}
}
#These two are subs for d_westminster
d_westminster_sub_1 = {
	pattern = "pattern_solid.dds"
	color1 = "yellow"
	color2 = "blue"
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = "red"
		color2 = "green"
		color3 = "yellow"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 }  }
	}
}

#Could also be used as House Wessex COA
d_westminster_sub_2 = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_cross_crosslet.dds"
		color1 = "yellow"
		instance = { position = { 0.5 0.45 } scale = { 1.0 1.2 } }
	}
	colored_emblem = {
		texture = "ce_martlet.dds"
		color1 = "yellow"
		color2 = "yellow"
		color3 = "yellow"
		instance = { position = { 0.25 0.25 } scale = { 0.2 0.2 } rotation = -10 } 
		instance = { position = { 0.25 0.75 } scale = { 0.2 0.2 } rotation = -10 } 
		instance = { position = { 0.75 0.25 } scale = { 0.2 0.2 } rotation = -10 } 
		instance = { position = { 0.75 0.75 } scale = { 0.2 0.2 } rotation = -10 } 
		instance = { position = { 0.5 0.9 } scale = { 0.2 0.2 } rotation = -10 }
	}
}

#This is the actual d_westminster COA
d_westminster = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "yellow"
	sub = {
        parent = "d_westminster_sub_1"
		instance = { offset = { 0.0 0.0 } scale = { 0.16 0.16 } }
		instance = { offset = { 0.31 0.0 } scale = { 0.16 0.16 } }
		instance = { offset = { 0.50 0.50 } scale = { 0.16 0.16 } }
		instance = { offset = { 0.81 0.50 } scale = { 0.16 0.16 } }
	}
	sub = {
        parent = "d_westminster_sub_2"
		instance = { offset = { 0.16 0.0 } scale = { 0.16 0.16 } }
		instance = { offset = { 0.66 0.50 } scale = { 0.16 0.16 } }
	}
	colored_emblem = {
		texture = "ce_portcullis.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.25 0.35 } scale = { 0.35 0.35 } }
		instance = { position = { 0.75 0.85 } scale = { 0.35 0.35 } }
	}
	colored_emblem = {
		texture = "ce_grain.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.75 0.25 } scale = { 0.5 0.5 } }
		instance = { position = { 0.25 0.75 } scale = { 0.5 0.5 } }
	}
}

d_sussex = {
	pattern = "pattern_horizontal_split_02.dds"
	color1 = "blue"
	color2 = "red"
	colored_emblem = {
		texture = "ce_martlet.dds"
		color1 = "yellow"
		color2 = "yellow"
		color3 = "yellow"
		instance = { position = { 0.3 0.45 } scale = { 0.3 0.3 } rotation = -10 }
		instance = { position = { 0.5 0.45 } scale = { 0.3 0.3 } rotation = -10 }
		instance = { position = { 0.7 0.45 } scale = { 0.3 0.3 } rotation = -10 }
		instance = { position = { 0.4 0.65 } scale = { 0.3 0.3 } rotation = -10 }
		instance = { position = { 0.6 0.65 } scale = { 0.3 0.3 } rotation = -10 }
		instance = { position = { 0.5 0.85 } scale = { 0.3 0.3 } rotation = -10 }
	}
	colored_emblem = {
		texture = "ce_waves_03.dds"
		color1 = "white"
		color2 = "white"
		instance = { position = { 0.5 0.3 } scale = { 1.5 0.25 } rotation = -45 }
	}
	colored_emblem = {
		texture = "ce_crown_head.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.5 0.15 } scale = { 0.3 0.3 } }
	}
}

d_norfolk = {
	pattern = "pattern_vertical_split_01.dds"
	color1 = "yellow"
	color2 = "black"
	colored_emblem = {
		texture = "ce_bend_thin.dds"
		color1 = "white"
		instance = { position = { 0.5 0.65 } scale { 1.0 1.0 } rotation = -5 }
	}
	colored_emblem = {
		texture = "ce_ermine_spot.dds"
		color1 = "black"
		instance = { position = { 0.2 0.4 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.35 0.45 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.315 0.55 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.475 0.65 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.6 0.665 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.55 0.75 } scale { 0.2 0.3 } rotation = -55 }
		instance = { position = { 0.8 0.775 } scale { 0.2 0.3 } rotation = -55 }
	}
	colored_emblem = {
		texture = "ce_block_03.dds"
		color1 = "red"
		instance = { position = { 0.5 0.0 } scale = { 2.0 2.5 } }
	}
	colored_emblem = {
		texture = "ce_lion_passant_guardant.dds"
		color1 = "yellow"
		color2 = "yellow"
		color3 = "red"
		instance = { position = { 0.5 0.16 } scale = { 0.6 0.35 } }
	}
	colored_emblem = {
		texture = "ce_religion_yazidism.dds"
		color1 = "yellow"
		color2 = "white"
		instance = { position = { 0.2 0.15 } scale = { 0.1 0.3 } }
		instance = { position = { 0.8 0.15 } scale = { 0.1 0.3 } }
	}
}

d_bedford = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "black"
	colored_emblem = {
		texture = "ce_block_03.dds"
		color1 = "black"
		instance = { position = { 0.5 0.0 } scale = { 2.0 2.0 } }
	}
	colored_emblem = {
		texture = "ce_clam.dds"
		color1 = "white"
		instance = { position = { 0.25 0.15 } scale = { 0.25 0.25 } }
		instance = { position = { 0.5 0.15 } scale = { 0.25 0.25 } }
		instance = { position = { 0.75 0.15 } scale = { 0.25 0.25 } }
	}
	colored_emblem = {
		texture = "ce_lion_rampant_per_pale.dds"
		color1 = "red"
		color2 = "blue"
		color3 = "white"
		instance = { position = { 0.5 0.6 } scale = { 1.0 0.7 } }
	}
}

d_somerset = {
pattern = "pattern_solid.dds"
	color1 = "yellow"
	color2 = "red"
	colored_emblem = {
		texture = "ce_ordinary_pile_01.dds"
		color1 = "red"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.2 } }
	}
	colored_emblem = {
		texture = "ce_lion_passant_guardant.dds"
		color1 = "yellow"
		color2 = "blue"
		color3 = "white"
		instance = { position = { 0.5 0.15 } scale = { 0.5 0.225 } }
		instance = { position = { 0.5 0.325 } scale = { 0.375 0.175 } }
		instance = { position = { 0.5 0.45 } scale = { 0.25 0.125 } }
	}
	colored_emblem = {
		texture = "ce_fleur.dds"
		color1 = "blue" 
		instance = { position = { 0.15 0.4 } scale = { 0.25 0.25 } }
		instance = { position = { 0.85 0.4 } scale = { 0.25 0.25 } }
		instance = { position = { 0.25 0.6 } scale = { 0.25 0.25 } }
		instance = { position = { 0.75 0.6 } scale = { 0.25 0.25 } }
		instance = { position = { 0.35 0.8 } scale = { 0.25 0.25 } }
		instance = { position = { 0.65 0.8 } scale = { 0.25 0.25 } }
	}
}

d_gloucester = {
	pattern = "pattern_solid.dds"
	color1 = "red"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_ordinary_chevron_3.dds"
		color1 = "yellow"
		instance = { position = { 0.5 0.75 } scale = { 1.0 1.0 } }
	}
	colored_emblem = {
		texture = "ce_block_01.dds"
		color1 = "yellow"
		instance = { position = { 0.5 0.0 } scale = { 1.0 1.0 } }
	}
	colored_emblem = {
		texture = "ce_block_02.dds"
		color1 = "blue"
		instance = { position = { 0.25 0.1 } scale = { 0.2 0.25 } }
		instance = { position = { 0.5 0.1 } scale = { 0.2 0.25 } }
		instance = { position = { 0.75 0.1 } scale = { 0.2 0.25 } }
	}
	colored_emblem = {
		texture = "ce_horse_shoe.dds"
		color1 = "yellow"
		instance = { position = { 0.25 0.1 } scale = { 0.2 0.25 } }
		instance = { position = { 0.5 0.1 } scale = { 0.2 0.25 } }
		instance = { position = { 0.75 0.1 } scale = { 0.2 0.25 } }
	}
	colored_emblem = {
		texture = "ce_sheep.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.25 0.4 } scale = { 0.3 0.3 } }
		instance = { position = { 0.75 0.4 } scale = { 0.3 0.3 } }
	}
}

d_hereford = {
	pattern = "pattern_solid.dds"
	color1 = "red"
	color2 = "white"
	colored_emblem = {
		texture = "ce_bull_head.dds"
		color1 = "white"
		color2 = "white"
		color3 = "white"
		instance = { position = { 0.5 0.8 } scale = { 0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_leopard_passant_guardant.dds"
		color1 = "white"
		color2 = "black"
		color3 = "white"
		instance = { position = { 0.5 0.2 } scale = { 1.0 0.9 } }
	}
	colored_emblem = {
		texture = "ce_waves_03.dds"
		color1 = "blue"
		color2 = "white"
		instance = { position = { 0.5 0.5 } scale = { 1.0 0.75 } rotation = -45 }
	}
}

d_lincoln = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "black"
	colored_emblem = {
		texture = "ce_block_01.dds"
		color1 = "blue"
		instance = { position = { 0.5 0.0 } scale = { 1.0 0.8 } }
	}
	colored_emblem = {
		texture = "ce_crosslet_fitchy.dds"
		color1 = "black"
		instance = { position = { 0.25 0.4 } scale = { 0.4 0.4 } }
		instance = { position = { 0.5 0.4 } scale = { 0.4 0.4 } }
		instance = { position = { 0.75 0.4 } scale = { 0.4 0.4 } }
		instance = { position = { 0.375 0.6 } scale = { 0.4 0.4 } }
		instance = { position = { 0.625 0.6 } scale = { 0.4 0.4 } }
		instance = { position = { 0.5 0.8 } scale = { 0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_star_05_pierced.dds"
		color1 = "yellow"
		color2 = "red"
		instance = { position = { 0.3 0.1 } scale = { 0.3 0.3 } }
		instance = { position = { 0.7 0.1 } scale = { 0.3 0.3 } }
	}
}

d_northumberland = {
	pattern = "pattern_checkers_02.dds"
	color1 = "blue"
	color2 = "red"
	colored_emblem = {
		texture = "ce_fess_lozenges_05.dds"
		color1 = "yellow"
		instance = { position = { 0.25 0.75 } scale = { 0.5 0.5 } }
		instance = { position = { 0.75 0.25 } scale = { 0.5 0.5 } }
	}
	colored_emblem = {
		texture = "ce_fish_03.dds"
		color1 = "white"
		color2 = "white"
		color3 = "black"
		instance = { position = { 0.375 0.125 } scale = { 0.2 0.2 } }
		instance = { position = { 0.125 0.375 } scale = { 0.2 0.2 } }
		instance = { position = { 0.625 0.875 } scale = { 0.2 0.2 } }
		instance = { position = { 0.875 0.625 } scale = { 0.2 0.2 } }
	}
	colored_emblem = {
		texture = "ce_block_02.dds"
		color1 = "yellow"
		instance = { position = { 0.125 0.125 } scale = { 0.25 0.25 } }
		instance = { position = { 0.375 0.375 } scale = { 0.25 0.25 } }
		instance = { position = { 0.625 0.625 } scale = { 0.25 0.25 } }
		instance = { position = { 0.875 0.875 } scale = { 0.25 0.25 } }
	}
	colored_emblem = {
		texture = "ce_lion_rampant.dds"
		color1 = "blue"
		color2 = "red"
		color3 = "black"
		instance = { position = { 0.125 0.125 } scale = { 0.25 0.25 } }
		instance = { position = { 0.375 0.375 } scale = { 0.25 0.25 } }
		instance = { position = { 0.625 0.625 } scale = { 0.25 0.25 } }
		instance = { position = { 0.875 0.875 } scale = { 0.25 0.25 } }
	}
}

d_welsh_march = {
	pattern = "pattern_checkers_02.dds"
	color1 = "red"
	color2 ="yellow"
	colored_emblem = {
		texture = "ce_dragon.dds"
		color1 = "yellow"
		color2 = "yellow"
		color3 = "white"
		instance = { position = { 0.25 0.75 } scale = { 0.5 0.5 } }
		instance = { position = { 0.75 0.25 } scale = { 0.5 0.5 } }
	}
	colored_emblem = {
		texture = "ce_dragon.dds"
		color1 = "red"
		color2 = "red"
		color3 = "white"
		instance = { position = { 0.25 0.25 } scale = { 0.5 0.5 } }
		instance = { position = { 0.75 0.75 } scale = { 0.5 0.5 } }
	}
}

d_wales = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "green"
	colored_emblem = {
		texture = "ce_lotus_flower.dds"
		color1 = "green"
		instance = { position = { 0.5 0.85 } scale = { 1.25 0.5 } }
	}
	colored_emblem = {
		texture = "ce_dragon.dds"
		color1 = "red"
		color2 = "blue"
		color3 = "white"
		instance = { position = { 0.5 0.5 } scale = { 1.25 1.0 } }
	}
}

d_buccleuch = {
	pattern = "pattern_bend_01.dds"
	color1 = "yellow"
	color2 = "blue"
	colored_emblem = {
		texture = "ce_crescent_mask_02.dds"
		color1 = "yellow"
		instance = { position = { 0.25 0.25 } scale = { 0.25 0.25 } rotation = 45 }
		instance = { position = { 0.75 0.75 } scale = { 0.25 0.25 } rotation = 45 }
	}
	colored_emblem = {
		texture = "ce_star_06.dds"
		color1 = "yellow"
		instance = { position = { 0.5 0.5 } scale = { 0.25 0.25 } }
	}
}

d_hamilton = {
	pattern = "pattern_checkers_02.dds"
	color1 = "white"
	color2 = "red"
	colored_emblem = {
		texture = "ce_cinquefoil.dds"
		color1 = "white"
		instance = { position = { 0.125 0.125 } scale = { 0.25 0.25 } }
		instance = { position = { 0.375 0.125 } scale = { 0.25 0.25 } }
		instance = { position = { 0.25 0.375 } scale = { 0.25 0.25 } }
		instance = { position = { 0.625 0.625 } scale = { 0.25 0.25 } }
		instance = { position = { 0.875 0.625 } scale = { 0.25 0.25 } }
		instance = { position = { 0.75 0.875 } scale = { 0.25 0.25 } }
	}
	colored_emblem = {
		texture = "ce_ship_lymphad.dds"
		color1 = "black"
		color2 = "black"
		color3 = "black"
		instance = { position = { 0.25 0.75 } scale = { 0.5 0.5 } }
		instance = { position = { 0.75 0.25 } scale = { 0.5 0.5 } }
	}
}

d_isle_of_man = {
	pattern = "pattern_solid.dds"
	color1 = "red"
	color2 = "white"
	colored_emblem = {
		texture = "ce_triskelion.dds"
		color1 = "white"
		color2 = "yellow"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 } }
	}
}

d_ross = {
	pattern = "pattern_solid.dds"
	color1 = "red"
	color2 = "white"
	colored_emblem = {
		texture = "ce_lion_rampant_per_pale.dds"
		color1 = "white"
		color2 = "white"
		color3 = "red"
		instance = { position = { 0.3 0.3 } scale = { 0.5 0.5 } }
		instance = { position = { 0.7 0.3 } scale = { 0.5 0.5 } }
		instance = { position = { 0.5 0.7 } scale = { 0.5 0.5 } }
	}
}

d_orkney = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_double_tressure.dds"
		color1 = "yellow"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 } }
	}
	colored_emblem = {
		texture = "ce_ship_lymphad.dds"
		color1 = "yellow"
		color2 = "red"
		color3 = "white" 
		instance = { position = { 0.5 0.5 } scale = { 0.5 0.5 } }
	}
}

d_thomond = {
	pattern = "pattern_solid.dds"
	color1 = "red"
	color2 = "white"
	colored_emblem = {
		texture = "ce_arm_holding_sword.dds"
		color1 = "white"
		color2 = "yellow"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 } }
	}
}

d_desmond = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "blue"
	colored_emblem = {
		texture = "ce_block_03.dds"
		color1 = "blue"
		instance = { position = { 0.5 0.5 } scale = { 1.5 1.1 } }
	}
	colored_emblem = {
		texture = "ce_lozenge.dds"
		color1 = "yellow"
		instance = { position = { 0.25 0.5 } scale = { 0.25 0.25 } }
		instance = { position = { 0.5 0.5 } scale = { 0.25 0.25 } }
		instance = { position = { 0.75 0.5 } scale = { 0.25 0.25 } }
	}
}

d_meath = {
	pattern = "pattern_solid.dds"
	color1 = "blue"
	color2 = "white"
	colored_emblem = {
		texture = "ce_ruler.dds"
		color1 = "yellow"
		color2 = "white"
		color3 = "green"
	}
}

d_leinster = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "red"
	colored_emblem = {
		texture = "ce_ordinary_saltire_5.dds"
		color1 = "red"
	}
}

d_ulster = {
	pattern = "pattern_cross_01.dds"
	color1 = "yellow"
	color2 = "red"
	colored_emblem = {
		texture = "ce_shield_01.dds"
		color1 = "white"
	}
	colored_emblem = {
		texture = "ce_hand.dds"
		color1 = "red"
	}
}

d_connacht = {
	pattern = "pattern_vertical_split_01.dds"
	color1 = "white"
	color2 = "blue"
	colored_emblem = {
		texture = "ce_eagle_double.dds"
		color1 = "black"
		color2 = "yellow"
		mask = { 1 }
	}
	colored_emblem = {
		texture = "ce_sword_simple.dds"
		color1 = "white"
		color2 = "white"
		instance = { position = { 0.7 0.5 } rotation = 15 } 
		mask = { 2 }
	}
}

d_the_isles = {
	pattern	= "pattern_solid.dds"
	color1 = "white"
	color2 = "black"
	colored_emblem = {
		texture = "ce_ship_lymphad.dds"
		color1 = "black"
		color2 = "red"
		color3 = "yellow"
	}
}

d_albany = {
	pattern = "pattern_solid.dds"
	color1 = "yellow"
	colored_emblem = {
		texture = "ce_lion_rampant.dds"
		color1 = "red"
		color2 = "blue"
		color3 = "black"
	}
}

d_york = {
	pattern = "pattern_cross_02.dds"
	color1 = "white"
	color2 = "red"
	color3 = "red"
	colored_emblem = {
		texture = "ce_lion_passant_guardant.dds"
		color1 = "yellow"
		color2 = "blue"
		color3 = "white"
		instance = { position = { 0.2 0.5 } scale = { 0.25 0.25 } }
		instance = { position = { 0.8 0.5 } scale = { 0.25 0.25 } }
		instance = { position = { 0.5 0.5 } scale = { 0.25 0.25 } }
		instance = { position = { 0.5 0.2 } scale = { 0.25 0.25 } }
		instance = { position = { 0.5 0.8 } scale = { 0.25 0.25 } }
	}
}

d_cornwall = {
	pattern = "pattern_solid.dds"
	color1 = "black"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_circle.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.10 0.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.30 0.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.50 0.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.70 0.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.90 0.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.00 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.20 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.40 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.60 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.80 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 1.00 0.20 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.10 0.40 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.30 0.40 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.50 0.40 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.70 0.40 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.90 0.40 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.00 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.20 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.40 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.60 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.80 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 1.00 0.60 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.10 0.80 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.30 0.80 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.50 0.80 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.70 0.80 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.90 0.80 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.00 1.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.20 1.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.40 1.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.60 1.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 0.80 1.00 } scale = { 0.2 0.2 }  }
		instance = { position = { 1.00 1.00 } scale = { 0.2 0.2 }  }
	}
}