special_tolerance = {
	group = "main_group"
	
	is_available_on_create = {
		always = no # Goes away when creating a new Faith
	}
	
	special_doctrine_ecumenical_christian = {
		parameters = {
			hostility_override_special_doctrine_ecumenical_christian = 1		
		}
	}
	
	special_doctrine_reformed_christian = {
		parameters = {
			hostility_override_special_doctrine_reformed_christian = 1			
		}
	}
	
	special_doctrine_radical_christian = {
		visible = no
		character_modifier = {
			advantage_against_coreligionists = 5
		}
	}
}
