﻿
#root winning as attacker

pm_war_trigger1 = {

	any_character_war = {

		attacker_war_score > 30
		attacker_war_score < 75
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
pm_war_trigger2 = {

	any_character_war = {

		attacker_war_score > 75
		attacker_war_score < 100
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
pm_war_trigger3 = {

	any_character_war = {

		attacker_war_score = 100
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
#root losing as attacker
pm_war_trigger4 = {

	any_character_war = {

		defender_war_score > 30
		defender_war_score < 75
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
pm_war_trigger5 = {

	any_character_war = {

		defender_war_score > 75
		defender_war_score < 100
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
pm_war_trigger6 = {

	any_character_war = {

		defender_war_score = 100
		
		casus_belli = {
			primary_attacker = scope:actor
			primary_defender = scope:recipient
		}
	}
}
#root winning as defender
pm_war_trigger7 = {

	any_character_war = {

		defender_war_score > 30
		defender_war_score < 75
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
pm_war_trigger8 = {

	any_character_war = {

		defender_war_score > 75
		defender_war_score < 100
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
pm_war_trigger9 = {

	any_character_war = {

		defender_war_score = 100
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
#root losing as defender
pm_war_trigger10 = {

	any_character_war = {

		attacker_war_score > 30
		attacker_war_score < 75
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
pm_war_trigger11 = {

	any_character_war = {

		attacker_war_score > 75
		attacker_war_score < 100
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
pm_war_trigger12 = {

	any_character_war = {

		attacker_war_score = 100
		
		casus_belli = {
			primary_attacker = scope:recipient
			primary_defender = scope:actor
		}
	}
}
pm_testo = {
	any_character_war = {
		casus_belli = {
			OR = {
				primary_attacker = {
					is_local_player = yes
				}
				primary_defender = {
					is_local_player = yes
				}								
			}
		}
	}
						
}