version="1.8.1"
tags={
	"Total Conversion"
	"Historical"
	"Map"
	"Gameplay"
}
replace_path="common/culture/innovations"
replace_path="common/bookmark_portraits"
replace_path="common/bookmarks"

replace_path="history"
replace_path="history/cultures"
replace_path="history/characters"
replace_path="history/provinces"
replace_path="history/titles"
replace_path="history/wars"

replace_path="map_data"
name="Crusader Universalis Alpha 0.1.6"
supported_version="1.9.*"