﻿# Korean {
	# Joseon = {
		korean001 = {
			name = "Seongye"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			1335.11.4 = {
				birth = yes
			}
			1408.6.27 = {
				name = "Taejo"
				death = yes
			}
		}
		korean002 = {
			name = "Banggwa"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean001
			1357.7.26 = {
				birth = yes
			}
			1419.10.24 = {
				name = "Jeongjong"
				death = yes
			}
		}
		korean003 = {
			name = "Bangwon"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean001
			1367.6.21 = {
				birth = yes
			}
			1422.6.8 = {
				name = "Taejong"
				death = yes
			}
		}
		korean004 = {
			name = "Do"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean003
			1397.5.15 = {
				birth = yes
			}
			1450.4.8 = {
				name = "Sejong"
				death = yes
			}
		}
		korean005 = {
			name = "Hyang"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean004
			1414.11.15 = {
				birth = yes
			}
			1452.6.10 = {
				name = "Munjong"
				death = yes
			}
		}
		korean006 = {
			name = "Hongwi"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean005
			1441.8.9 = {
				birth = yes
			}
			1457.11.16 = {
				name = "Danjong"
				death = {
					death_reason = death_murder
				}
			}
		}
		korean007 = {
			name = "Yu"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean004
			1417.11.2 = {
				birth = yes
			}
			1468.9.23 = {
				name = "Sejo"
				death = yes
			}
		}
		korean008 = {
			name = "Jang"
			dynasty = CKU_joseon
			religion = test_faith #lixue
			culture = test_culture
			father = korean007
			1438.10.3 = {
				birth = yes
			}
			1457.9.20 = {
				name = "Deokjong"
				death = {
					death_reason = death_ill
				}
			}
		}
	# }
# }