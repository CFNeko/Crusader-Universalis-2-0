1012 = {	#iveagh
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1013 = {	#armagh
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1014 = {	#derry
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1015 = {	#newry
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1016 = {	#clanawley
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1017 = {	#monaghan
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1018 = {	#fermanagh
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1019 = {	#coleraine
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1020 = {	#down
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1021 = {	#tullyhaw
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1022 = {	#donegal
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1023 = {	#tyrone
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1024 = {	#cavan
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1025 = {	#carrickfergus
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1026 = {	#antrim
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1027 = {	#farney
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1028 = {	#fingal
	980.1.1 = {
		culture = hiberno_norman
		religion = catholic
		holding = castle_holding
	}
}
1029 = {	#kildare
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1030 = {	#carlow
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1031 = {	#wicklow
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1032 = {	#wexford
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1033 = {	#laois
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1034 = {	#kilkenny
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1035 = {	#ferns
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1036 = {	#callan
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1037 = {	#dublin
	980.1.1 = {
		culture = hiberno_norman
		religion = catholic
		holding = city_holding
	}
}
1038 = {	#westmeath
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1039 = {	#irish_louth
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1040 = {	#offaly
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1041 = {	#meath
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1042 = {	#longford
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1043 = {	#waterford
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1044 = {	#nenagh
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1045 = {	#clonmel
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1046 = {	#clare
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1047 = {	#clancullen
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1048 = {	#tipperary
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1049 = {	#athenry
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1050 = {	#mayo
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1051 = {	#roscommon
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1052 = {	#westport
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1053 = {	#galway
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1054 = {	#gailenga
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1055 = {	#leitrim
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1056 = {	#sligo
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1057 = {	#boyle
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1058 = {	#tuam
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = church_holding
	}
}
1059 = {	#kerry
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1060 = {	#clangibbon
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1061 = {	#mccarthy_mor
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1062 = {	#cork
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1063 = {	#muskerry
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}
1064 = {	#limerick
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}
1065 = {	#askeaton
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}

1066 = {	#askeaton
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}

1067 = {	#imokilly
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = castle_holding
	}
}

1068 = {	#kinsale
	980.1.1 = {
		culture = irish
		religion = catholic
		holding = city_holding
	}
}