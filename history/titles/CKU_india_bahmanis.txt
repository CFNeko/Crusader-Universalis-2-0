#bahmanis
	k_ahmednagar = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			
		}
	}
	k_berar = {
		1444.1.1 = {
			holder = CKU_bahmani_1
		}
	}
	k_bijapur = {
		1444.1.1 = {
			holder = CKU_bahmani_1
		}
	}
	d_bidar = {
		1444.1.1 = {
			holder = CKU_bahmani_1
		}
	}
	c_bidar = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 16
		}
	}
	c_yadgir = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 15
		}
	}
	c_aland = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 15
		}
	}
	c_marolia = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 12
		}
	}
	c_patan = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 10
		}
	}
	c_kandahar = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 9
		}
	}
	c_nizamabad = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 8
		}
	}
	c_koratla = {
		1444.1.1 = {
			holder = CKU_bahmani_1
			change_development_level = 7
		}
	}
#mudgal
	d_raichur = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			liege = k_ahmednagar
			
		}
	}
	c_raichuru = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			change_development_level = 17
		}
	}
	c_gadwal = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			change_development_level = 16
		}
	}
	c_shorapur = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			change_development_level = 15
		}
	}
	c_mudgal = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			change_development_level = 15
		}
	}
	c_koppla = {
		1444.1.1 = {
			holder = CKU_mudgal_1
			change_development_level = 14
		}
	}
#savanur
	d_savanur = {
		1444.1.1 = {
			holder = CKU_savanur_1
			liege = k_ahmednagar
		}
	}
	c_savanur = {
		1444.1.1 = {
			holder = CKU_savanur_1
			change_development_level = 11
		}
	}
	c_hubli = {
		1444.1.1 = {
			holder = CKU_savanur_1
			change_development_level = 4
		}
	}
	c_gajendragarh = {
		1444.1.1 = {
			holder = CKU_savanur_1
			change_development_level = 10
		}
	}
	c_gudag = {
		1444.1.1 = {
			holder = CKU_savanur_1
			change_development_level = 10
		}
	}
#bijapur
	d_bijapur = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			liege = k_ahmednagar
			
		}
	}
	c_bijapur = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 14
		}
	}
	c_akluj = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 12
		}
	}
	c_athani = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 11
		}
	}
	c_miraj = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 10
		}
	}
	c_belgaon = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 11
		}
	}
	c_jamkhandi = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 10
		}
	}
	c_kolhapur = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 8
		}
	}
	c_ilkal = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 12
		}
	}
	c_karad = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 9
		}
	}
	c_satara = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 9
		}
	}
	c_pandharpur = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 10
		}
	}
	c_bhor = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 8
		}
	}
	c_sangli = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 7
		}
	}
	c_ratnagiri = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 6
		}
	}
	c_malvan = {
		1444.1.1 = {
			holder = CKU_bijapur_1
			change_development_level = 7
		}
	}
#ahmednagar
	d_ahmednagar = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			liege = k_ahmednagar
			
		}
	}
	c_ahmednagar = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 12
		}
	}
	c_puna = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 9
		}
	}
	c_kormala = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 9
		}
	}
	c_solapur = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 8
		}
	}
	c_osmanabad = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 7
		}
	}
	c_barsi = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 6
		}
	}
	c_purli = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 6
		}
	}
	c_shrirampur = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 6
		}
	}
	c_pathardi = {
		1444.1.1 = {
			holder = CKU_ahmednagar_1
			change_development_level = 7
		}
	}
#daulatabad
	c_daulatabad = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 12
			liege = k_ahmednagar
			
		}
	}
	c_gourithapetra = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 8
		}
	}
	c_nandgaon = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 10
		}
	}
	c_nashik = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 9
		}
	}
	c_bhadgaon = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 9
		}
	}
	c_chersithlia = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 5
		}
	}
	c_malkapur = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 6
		}
	}
	c_hihieresmos = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 6
		}
	}
	c_gorgorooas = {
		1444.1.1 = {
			holder = CKU_daulatabad_1
			change_development_level = 5
		}
	}
#berar
	d_berar = {
		1444.1.1 = {
			holder = CKU_berar_1
			liege = k_ahmednagar
			
		}
	}
	c_amaravati = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 9
		}
	}
	c_manwat = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 8
		}
	}
	c_bhainsa = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 8
		}
	}
	c_nirmal = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 8
		}
	}
	c_yeotmal = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 7
		}
	}
	c_akola = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 7
		}
	}
	c_magarsaea = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 6
		}
	}
	c_chimonastiaea = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 6
		}
	}
	c_halicalinlia = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 5
		}
	}
	c_bougolcum = {
		1444.1.1 = {
			holder = CKU_berar_1
			change_development_level = 5
		}
	}
#nagpur
	d_nagpur = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			liege = k_ahmednagar
			
		}
	}
	c_nagpur = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			change_development_level = 11
		}
	}
	c_gondia = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			change_development_level = 11
		}
	}
	c_balaghat = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			change_development_level = 9
		}
	}
	c_warora = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			change_development_level = 9
		}
	}
	c_kerkdimathera = {
		1444.1.1 = {
			holder = CKU_nagpur_1
			change_development_level = 6
		}
	}
#golconda
	d_golconda = {
		1444.1.1 = {
			holder = CKU_golconda_1
			liege = k_ahmednagar
			
		}
	}
	c_golconda = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 12
		}
	}
	c_mahbubnagarh = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 11
		}
	}
	c_medak = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 9
		}
	}
	c_hyderabad = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 8
		}
	}
	c_kazipet = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 8
		}
	}
	c_mahbubabad = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 7
		}
	}
	c_warangal = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 8
		}
	}
	c_parkal = {
		1444.1.1 = {
			holder = CKU_golconda_1
			change_development_level = 6
		}
	}
#nalgonda
	d_nalgonda = {
		1444.1.1 = {
			holder = CKU_telingana_1
			liege = k_ahmednagar
		}
	}
	c_nalgonda = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 11
		}
	}
	c_devaraconda = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 10
		}
	}
	c_khammam = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 8
		}
	}
	c_yellandu = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 7
		}
	}
	c_kottagudam = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 8
		}
	}
	c_north_godavari = {
		1444.1.1 = {
			holder = CKU_telingana_1
			change_development_level = 8
		}
	}
#karjat
	c_junnar = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 7
			liege = k_ahmednagar
		}
	}
	c_sinnar = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 8
		}
	}
	c_lonawla = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 5
		}
	}
	c_croaskios = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 4
		}
	}
	c_murud = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 5
		}
	}
	c_shrivardhan = {
		1444.1.1 = {
			holder = CKU_karjat_1
			change_development_level = 5
		}
	}