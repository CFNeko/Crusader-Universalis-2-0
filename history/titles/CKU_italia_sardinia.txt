k_lisbythmos = { # Sardinia
	1443.1.1 = {
		government = feudal_government
		holder = CKU_borgona_250
	}
}

#Corsica
c_bonifacio = {
	1444.1.1 = {
		change_development_level = 6
		government = feudal_government
		liege = e_hre
		holder = CKU_adorno_1
	}
}
c_bastia = {
	1444.1.1 = {
		change_development_level = 5
		government = feudal_government
		liege = e_hre
		holder = CKU_adorno_1
	}
}
c_ajaccio = {
	1444.1.1 = {
		change_development_level = 8
		government = feudal_government
		liege = e_hre
		holder = CKU_adorno_1
	}
}

#Sardinia
c_quirra = {
	1443.1.1 = {
		change_development_level = 6
		government = feudal_government
		holder = CKU_carroz_18
		liege = k_lisbythmos
	}
}
c_carbonia = {
	1443.1.1 = {
		change_development_level = 6
		government = feudal_government
		holder = CKU_borgona_250
	}
}
c_arborea = {
	1443.1.1 = {
		change_development_level = 5
		government = feudal_government
		holder = CKU_borgona_250
	}
}
c_gallura = {
	1443.1.1 = {
		change_development_level = 6
		government = feudal_government
		holder = CKU_borgona_250
	}
}
c_cagliari = {
	1443.1.1 = {
		change_development_level = 8
		government = feudal_government
		holder = CKU_borgona_250
	}
}
c_torres = {
	1443.1.1 = {
		change_development_level = 8
		government = feudal_government
		holder = CKU_borgona_250
	}
}
c_goceano = {
	1443.1.1 = {
		change_development_level = 5
		government = feudal_government
		holder = CKU_borgona_250
	}
}