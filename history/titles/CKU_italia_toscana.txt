#Firenze
d_firenze = {
	1444.1.1 = {
		government = feudal_government
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_florence = {
	1444.1.1 = {
		change_development_level = 5		
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_pistoia = {
	1444.1.1 = {
		change_development_level = 6		
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_arezzo = {
	1444.1.1 = {
		change_development_level = 10
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_montevarchi = {
	1444.1.1 = {
		change_development_level = 10
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_empoli = {
	1444.1.1 = {
		change_development_level = 14
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_carrara = {
	1444.1.1 = {
		change_development_level = 6
		government = feudal_government
		liege = e_hre
		holder = CKU_visconti_53
	}
}
c_lucca = {
	1444.1.1 = {
		change_development_level = 11
		government = feudal_government
		liege = e_hre
		holder = CKU_lucca_1
	}
}

#Pisa
c_volterra = {
	1444.1.1 = {
		change_development_level = 11		
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_pisa = {
	1444.1.1 = {
		change_development_level = 7	
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_pontedera = {
	1444.1.1 = {
		change_development_level = 5
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_livorno = {
	1444.1.1 = {
		change_development_level = 5
		liege = e_hre
		holder = CKU_medici_77
	}
}

#Siena
d_oropoeera = {
	1444.1.1 = {
		government = feudal_government
		liege = e_hre
		holder = CKU_siena_1
	}
}
c_siena = {
	1444.1.1 = {
		change_development_level = 11
		liege = e_hre
		holder = CKU_siena_1
	}
}
c_grosseto = {
	1444.1.1 = {
		change_development_level = 6
		liege = e_hre
		holder = CKU_siena_1
	}
}
c_santa_fiora = {
	1444.1.1 = {
		change_development_level = 7
		holder = CKU_sforza_11
		liege = e_hre
	}
}
c_montepulciano = {
	1444.1.1 = {
		change_development_level = 10
		liege = e_hre
		holder = CKU_medici_77
	}
}
c_piombino = {
	1444.1.1 = {
		change_development_level = 5
		liege = e_hre
		holder = CKU_colonna_1155
	}
}